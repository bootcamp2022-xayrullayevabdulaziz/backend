package main

import (
    "fmt"
    "time"
	
)

func main() {
    dobStr := "11.05.2000" // Replace this date with your birthday
    givenDate, err := time.Parse("02.01.2006", dobStr)
    if err != nil {
        panic(err)
    }
    fmt.Printf("%s is %s", givenDate.Format("02-01-2006"), FindWeekday(givenDate))
}

func FindWeekday(date time.Time) (weekday string) {
    var day,mon,year int= date.Day(),int(date.Month()),date.Year()
	mon=(mon+10)%12
cen:=year/100
year=year%100

week:=(day+int(2.6*float32(mon)-0.2)-2*cen+year+year/4+cen/4)%7
if(week<0){
	week=week+7
}
switch week {
case 0:
	weekday="Sunday"
case 1:
	weekday="Monday"
case 2:
	weekday="Tuesday"
case 3:
	weekday="Wednesday"
case 4:
	weekday="Thursday"
case 5:
	weekday="Friday"
case 6:
	weekday="Saturday"

}
return weekday
}