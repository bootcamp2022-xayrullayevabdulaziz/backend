package main

import "fmt"

func main() {
  var a, b int

  fmt.Scanf("%d", &a)
  fmt.Scanf("%d", &b)
  fmt.Printf("a = %d, b = %d\n", a, b)

  a=a-b
  b=b+a
  a=b-a
 
  fmt.Printf("a = %d, b = %d\n", a, b)
}