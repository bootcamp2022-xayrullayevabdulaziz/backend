package main

import (
    "fmt"
)

func main() {
   // fmt.Printf("%.5f",MySquareRoot(10, 5))
	fmt.Printf("%f",MySquareRoot(10, 3))
}

func MySquareRoot(num, precision uint) (result float64) {
 
    var start , end uint=0, num;
    var mid uint
	
	for (start <= end) {
        mid = (start + end) / 2;
        if (mid * mid == num) {
            result = float64(mid);
            break;
        }
 
        
        if (mid * mid < num) {
            start = mid + 1;
            result = float64(mid);
        }else {
            end = mid - 1;
        }
 
       
        
    }

	increment:= 0.1;
    for i:= uint(0); i < precision; i++ {
		
        for result * result <=float64(num) {
			
            result += increment;
        }
 
        
       result = result - increment;
		increment = increment / 10;
		
    }
	
   
    return result
}