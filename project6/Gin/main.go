package main

import (
	"log"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type Person struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}
type Content struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}
type Article struct {
	ID        int `json:"id"`
	Content   `json:"content"`
	Author    Person     `json:"age"`
	CreatedAt *time.Time `json:"time"`
}

var ArticleStorage map[int]Article

func main() {
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())
	r.StaticFS("/static", gin.Dir("static", false))
	ArticleStorage = make(map[int]Article)
	time := time.Now()
	ArticleStorage[1] = Article{
		ID: 1,
		Content: Content{
			Title: "sarlavha1",
			Body:  "Tana1",
		},
		Author: Person{
			Firstname: "Fname1",
			Lastname:  "LName1",
		},
		CreatedAt: &time,
	}
	ArticleStorage[2] = Article{
		ID: 2,
		Content: Content{
			Title: "sarlavha2",
			Body:  "Tana2",
		},
		Author: Person{
			Firstname: "Fname2",
			Lastname:  "LName2",
		},
		CreatedAt: &time,
	}
	ArticleRoutes := r.Group("/articles")
	{
		ArticleRoutes.GET("/", getArticles)
		ArticleRoutes.GET("/:id", getArticlesById)
		ArticleRoutes.POST("/", postArtcle)
		ArticleRoutes.PUT("/", editArticle)
		ArticleRoutes.DELETE("/:id", deleteArticleById)
	}

	if err := r.Run(":3000"); err != nil {
		log.Fatal(err.Error())
	}
}
func getArticles(c *gin.Context) {
	var store []Article
	for _, v := range ArticleStorage {
		store = append(store, v)
	}

	c.JSON(200, gin.H{
		"message":  "it is get request",
		"articles": store,
	})
}
func getArticlesById(c *gin.Context) {
	id := c.Param("id")
	i, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(404, gin.H{
			"error":   true,
			"message": "invalid user id",
		})
	}
	selectedData := ArticleStorage[i]
	c.JSON(200, gin.H{
		"error":    false,
		"yourDate": selectedData,
	})
}
func editArticle(c *gin.Context) {
	var reqBody Article
	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.JSON(422, gin.H{
			"error":   true,
			"message": "invalid request body",
		})
		return
	}
	_, ok := ArticleStorage[reqBody.ID]
	if !ok {
		c.JSON(403, gin.H{
			"error":   true,
			"message": "There is not this id",
		})
		return
	}
	ArticleStorage[reqBody.ID] = reqBody
	c.JSON(200, gin.H{
		"error":   false,
		"message": "this data successfuly edited",
	})
}
func postArtcle(c *gin.Context) {
	var reqBody Article
	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.JSON(422, gin.H{
			"error":   true,
			"message": "invalid request body",
		})
		return
	}
	_, ok := ArticleStorage[reqBody.ID]
	if ok {
		c.JSON(403, gin.H{
			"error":   true,
			"message": "This id already exist",
		})
		return
	}
	ArticleStorage[reqBody.ID] = reqBody
	c.JSON(200, gin.H{
		"error":   false,
		"message": "this data successfuly added",
	})
}


func deleteArticleById(c *gin.Context) {
	id := c.Param("id")
	i, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(404, gin.H{
			"error":   true,
			"message": "invalid user id",
		})
	}
	_, ok := ArticleStorage[i]
	if !ok {
		c.JSON(403, gin.H{
			"error":   true,
			"message": "This id doesnt exist",
		})
		return
	}
	delete(ArticleStorage, i)
	c.JSON(200, gin.H{
		"error":    false,
		"message":"dataDeleted",
	})

	
}
