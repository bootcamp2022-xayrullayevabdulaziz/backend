package main

import (
	"log"
	"net/http"
)

func main() {

	http.Handle("/", http.FileServer(http.Dir("./static")))
	log.Println("server running on 443....")
    log.Fatal(http.ListenAndServeTLS(":443", "server.crt", "server.key", nil))

}
// http.Handle("/", http.FileServer(http.Dir("./static")))
// log.Println("server running on 8081....")
// log.Fatal(http.ListenAndServe(":8081", nil))