import React from "react";

import {Row,Col,Form,Input,Button,Card,CardHeader,CardBody,CardFooter,Container} from "reactstrap";

import axios from "axios"
var BASE_URL="http://localhost:3001/"


class Books extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            books: [],
            selectedBook: {}
        }
    }


    componentWillMount() {
        axios.get(BASE_URL)
            .then(response => {
                console.log(response.data);
                this.setState({
                    books: response.data
                })
            })
    }

    render() {
 

        const deleteBook = (id) => {
         
            axios.delete(BASE_URL + id)
                .then(response => {
                    console.log(response)
                    this.setState({
                        books: response.data
                    })
                })
        };

        const addBook = (e) => {
            e.preventDefault();

            var BookName = e.target.bookName.value;
            var BookPrice = e.target.bookPrice.value;
            var BookExpireDate = e.target.bookExpireDate.value;
            var BookAuthor = e.target.bookAuthor.value;
           

            var newBook = {
                name: BookName,
                cost: BookPrice,
                published: BookExpireDate,
                author: BookAuthor,
               
            };
            e.target.reset();


            axios.post(BASE_URL, newBook)
                .then(response => {
                    console.log(response);
                    this.setState({
                        books: response.data
                    })
                })
        };

        const editBook = (book) => {
            this.setState({
                selectedBook: book
            })
        };

        const editedBook = (e) => {
            e.preventDefault();

            var BookName = e.target.bookName.value;
            var BookPrice = e.target.bookPrice.value;
            var BookExpireDate = e.target.bookExpireDate.value;
            var BookAuthor = e.target.bookAuthor.value;

            var newBook = {
                name: BookName,
                cost: BookPrice,
                published: BookExpireDate,
                author: BookAuthor,
               
            };

            e.target.reset();

            axios.put(BASE_URL+ this.state.selectedBook.id, newBook)
                .then(response => {
                    console.log(response);
                    
                    this.setState({
                        books: response.data,
                        selectedBook: {}
                    })
                })
        };


        return (
            <Container>
                <Row>
                    <Col md={{size: 6, offset: 3}}>
                        <Form onSubmit={ this.state.selectedBook.name ?editedBook : addBook}>
                            <Input defaultValue={this.state.selectedBook.name ? this.state.selectedBook.name : ""}
                                   type="text" placeholder="Name" name="bookName"/>
                            <Input
                                defaultValue={this.state.selectedBook.cost ? this.state.selectedBook.cost : ""}
                                type="number" placeholder="Price" name="bookPrice" className="mt-3"/>
                            <Input
                                defaultValue={this.state.selectedBook.published ? this.state.selectedBook.published : ""}
                                type="date" placeholder="Expire Date" name="bookExpireDate" className="mt-3"/>
                            
                            
                            <Input defaultValue={this.state.selectedBook.author ? this.state.selectedBook.author : ""}
                                   type="text" placeholder="author" name="bookAuthor" className="mt-3"/>
                            {this.state.selectedBook.name ?
                                <Button color="primary" className="mt-3 float-right">Edit</Button> :
                                <Button color="success" className="mt-3 float-right">Add</Button>
                            }
                        </Form>
                    </Col>
                </Row>
                <Row>

                    {this.state.books.map(book => (
                        <Col md="4" className="mt-3">
                            <Card>
                                <CardHeader>
                                    <h4>{book.name}</h4>
                                </CardHeader>
                                <CardBody>
                                    <p>Author: <b>{book.author}</b></p>
                                    <p>Price: <b>{book.cost}$</b></p>
                                    <p>Expire Date: <b>{book.published}</b></p>
                                   
                                </CardBody>
                                <CardFooter>
                                    <Button color="primary" onClick={() => editBook(book)}>Edit</Button>
                                    <Button color="danger" className="float-right"
                                            onClick={() => deleteBook(book.id)}>Delete</Button>
                                </CardFooter>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </Container>
        )
    }
}

export default Books