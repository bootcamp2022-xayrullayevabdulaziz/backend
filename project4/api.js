const app=require('express')()
var books=require('./books')
const uuid=require('uuid')
const express = require('express')
var cors=require('cors')
//bodydagi jsondi o'qish uchun 
app.use(express.json())
app.use(cors())


// get all date
app.get('/',(req,res)=>{
    res.send(books)
})

// get a book which is selected by user(read)
app.get('/api/books/:id',(req,res)=>{
    books=books.filter(book=>book.id==req.params.id)
    res.send(books)
})
// add book which is given by user(post)
app.post('/',(req,res)=>{
    if(!req.body.name||!req.body.published||!req.body.cost||!req.body.author){
        return   res.status(404).json({message:"hamma ma'lumotlarni to'ldiring"})
       }
       
        var newBook={
            id:uuid.v4(),
            name:req.body.name,
            author:req.body.author,
            published:req.body.published,
            cost:req.body.cost
        }
        books.push(newBook)
        res.json(books)
       
 
 
    
   
})
// edit book's date by using selected id
app.put('/:id',(req,res)=>{
    books.forEach((book)=>{
        if(book.id==req.params.id){
            book.name=req.body.name
            book.author=req.body.author
            book.published=req.body.published
            book.cost=req.body.cost
        }
    })
    res.send(books)
})
// delete book by using selected id
app.delete('/:id',(req,res)=>{
    const { id } = req.params;

    const projectIndex = books.findIndex(p => p.id == id);
   
    books.splice(projectIndex, 1);
   
    return res.send(books);

   
})
app.listen(3001,()=>{console.log("server running on 3001 ...")})
























