const books=[
    { 
        id:1,
        name:"o'tgan kunlar",
        author:"Abulla qodiriy",
        published:2000,
        cost:15000
    },
    {
        id:2,
        name:"sariq dev",
        author:"Xuduyberdi to'xtaboyev",
        published:2003,
        cost:18000
    },
    {
        id:3,
        name:"bolalik",
        author:"Zulfiya",
        published:2010,
        cost:25000
    },
    {
        id:4,
        name:"falsafa",
        author:"Rustam Yoqubov",
        published:1990,
        cost:21000
    }

]
module.exports=books