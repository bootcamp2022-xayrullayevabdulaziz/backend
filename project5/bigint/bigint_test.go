package bigint

import (
	//"fmt"

	"testing"
)

func TestAddTD(t *testing.T) {
	tests := []struct {
		label  string
		a, b   string
		wanted Bigint
	}{
		{"Test 1", "120", "211", Bigint{Number: []int16{3, 3, 1}}},
		{"Test 2", "-1", "1", Bigint{Number: []int16{0}}},
		{"Test 3", "25469874154789562458795482315445", "48759587469854128745123654", Bigint{Number: []int16{2, 5, 4, 6, 9, 9, 2, 2, 9, 1, 4, 3, 7, 7, 0, 3, 2, 3, 1, 2, 9, 2, 4, 2, 2, 7, 4, 3, 9, 0, 9, 9}}},
		{"Test 4", "25469874154789562458795482315445", "-48759587469854128745123654", Bigint{Number: []int16{2, 5, 4, 6, 9, 8, 2, 5, 3, 9, 5, 2, 0, 2, 0, 9, 2, 6, 0, 4, 6, 6, 6, 7, 3, 7, 1, 9, 1, 7, 9, 1}}},
		{"Test 5", "-120", "200", Bigint{Number: []int16{8, 0}}},
		{"Test 6", "200", "-300", Bigint{Number: []int16{-1, 0, 0}}},
		{"Test 7", "-10", "-20", Bigint{Number: []int16{-3, 0}}},
		{"Test 8", "50", "50", Bigint{Number: []int16{1, 0, 0}}},
		{"Test 9", "10", "1000", Bigint{Number: []int16{1, 0, 1, 0}}},
	}
	for _, tt := range tests {
		t.Run(tt.label, func(t *testing.T) {
			a, _ := NewInt(tt.a)
			b, _ := NewInt(tt.b)
			ans := Add(a, b)

			for i := 0; i < len(ans.Number); i++ {
				if ans.Number[i] != tt.wanted.Number[i] {
					t.Errorf("got %q wanted %q", ans.Number, tt.wanted.Number)
					break
				}

			}

		})
	}
}

func TestNewInt(t *testing.T) {
	tests := []struct {
		label  string
		a      string
		wanted Bigint
		err    error
	}{
		{"Test 1", "2541", Bigint{Number: []int16{2, 5, 4, 1}}, nil},
		{"Test 2", "+210", Bigint{Number: []int16{2, 1, 0}}, nil},

		{"Test 3", "-12", Bigint{Number: []int16{-1, 2}}, nil},
		{"Test 4", "000154", Bigint{Number: []int16{1, 5, 4}}, nil},
		{"Test 5", "0000", Bigint{Number: []int16{0}}, nil},
		{"Test 6", "231g", Bigint{Number: []int16{}}, ErrorMessage},
		{"Test 7", "-0000010", Bigint{Number: []int16{-1, 0}}, nil},
		{"Test 8", "+00010", Bigint{Number: []int16{1, 0}}, nil},
	}

	for _, tt := range tests {

		t.Run(tt.label, func(t *testing.T) {

			ans, err := NewInt(tt.a)

			if err != tt.err {
				t.Errorf("Error: %e", err)
			} else {
				for i := 0; i < len(ans.Number); i++ {
					if ans.Number[i] != tt.wanted.Number[i] {
						t.Errorf("got %q wanted %q", ans.Number, tt.wanted.Number)
						break
					}

				}
			}

		})

	}
}

func TestSet(t *testing.T) {
	tests := []struct {
		label  string
		num    string
		wanted Bigint
		err    error
	}{
		{"Test 1", "10", Bigint{Number: []int16{1, 0}}, nil},
		{"Test 2", "-0000010", Bigint{Number: []int16{-1, 0}}, nil},
		{"Test 3", "+00010", Bigint{Number: []int16{1, 0}}, nil},
		{"Test 4", "2a", Bigint{Number: []int16{}}, ErrorMessage},
	}
	for _, tt := range tests {

		t.Run(tt.label, func(t *testing.T) {
			a, _ := NewInt("126")
			err := a.Set(tt.num)
			if err != tt.err {
				t.Errorf("Expected %v Got %v", tt.err, err)

			}

		})

	}
}

func TestSub(t *testing.T) {
	tests := []struct {
		label  string
		a, b   string
		wanted Bigint
	}{
		{"Test 1", "150", "100", Bigint{Number: []int16{5, 0}}},
		{"Test 2", "-1", "1", Bigint{Number: []int16{-2}}},
		{"Test 3", "25469874154789562458795482315445", "48759587469854128745123654", Bigint{Number: []int16{2, 5, 4, 6, 9, 8, 2, 5, 3, 9, 5, 2, 0, 2, 0, 9, 2, 6, 0, 4, 6, 6, 6, 7, 3, 7, 1, 9, 1, 7, 9, 1}}},
		{"Test 4", "25469874154789562458795482315445", "-48759587469854128745123654", Bigint{Number: []int16{2, 5, 4, 6, 9, 9, 2, 2, 9, 1, 4, 3, 7, 7, 0, 3, 2, 3, 1, 2, 9, 2, 4, 2, 2, 7, 4, 3, 9, 0, 9, 9}}},
		{"Test 5", "120", "200", Bigint{Number: []int16{-8, 0}}},
		{"Test 6", "-200", "-300", Bigint{Number: []int16{1, 0, 0}}},
		{"Test 7", "-10", "-20", Bigint{Number: []int16{1, 0}}},
		{"Test 8", "1", "1", Bigint{Number: []int16{0}}},
	}
	for _, tt := range tests {
		t.Run(tt.label, func(t *testing.T) {
			a, _ := NewInt(tt.a)
			b, _ := NewInt(tt.b)
			ans := Sub(a, b)

			for i := 0; i < len(ans.Number); i++ {
				if ans.Number[i] != tt.wanted.Number[i] {
					t.Errorf("got %q wanted %q", ans.Number, tt.wanted.Number)
					break
				}

			}
		})
	}

}

func TestMultiply(t *testing.T) {
	tests := []struct {
		label  string
		a, b   string
		wanted Bigint
	}{
		{"Test 1", "0", "100", Bigint{Number: []int16{0}}},
		{"Test 2", "-1", "1", Bigint{Number: []int16{-1}}},
		{"Test 3", "25469874154789562458795482315445", "48759587469854128745123654", Bigint{Number: []int16{1, 2, 4, 1, 9, 0, 0, 5, 5, 6, 6, 9, 6, 6, 3, 8, 6, 6, 7, 6, 4, 8, 1, 6, 4, 8, 2, 4, 8, 8, 2, 7, 4, 9, 5, 7, 5, 2, 3, 6, 7, 4, 5, 6, 2, 8, 5, 3, 6, 7, 5, 9, 0, 3, 6, 0, 3, 0}}},
		{"Test 4", "25469874154789562458795482315445", "-48759587469854128745123654", Bigint{Number: []int16{-1, 2, 4, 1, 9, 0, 0, 5, 5, 6, 6, 9, 6, 6, 3, 8, 6, 6, 7, 6, 4, 8, 1, 6, 4, 8, 2, 4, 8, 8, 2, 7, 4, 9, 5, 7, 5, 2, 3, 6, 7, 4, 5, 6, 2, 8, 5, 3, 6, 7, 5, 9, 0, 3, 6, 0, 3, 0}}},
		{"Test 5", "-10", "-5", Bigint{Number: []int16{5, 0}}},
	}
	for _, tt := range tests {
		t.Run(tt.label, func(t *testing.T) {
			a, _ := NewInt(tt.a)
			b, _ := NewInt(tt.b)
			ans := Multiply(a, b)
			for i := 0; i < len(ans.Number); i++ {
				if ans.Number[i] != tt.wanted.Number[i] {
					t.Errorf("got %q wanted %q", ans.Number, tt.wanted.Number)
					break
				}

			}

		})
	}

}

func TestDivitionMod(t *testing.T) {
	tests := []struct {
		label   string
		a, b    string
		wantedM Bigint
		wantedD Bigint
		err     error
	}{
		{"Test 1", "20", "100", Bigint{Number: []int16{2, 0}}, Bigint{Number: []int16{0}}, nil},
		{"Test 2", "30", "8", Bigint{Number: []int16{6}}, Bigint{Number: []int16{3}}, nil},
		{"Test 3", "-30", "8", Bigint{Number: []int16{-6}}, Bigint{Number: []int16{-3}}, nil},
		{"Test 3", "-30", "-8", Bigint{Number: []int16{-6}}, Bigint{Number: []int16{3}}, nil},
		{"Test 4", "52", "0", Bigint{Number: []int16{}}, Bigint{Number: []int16{}}, ErrorMessage},
		{"Test 5", "25469874154789562458795482315445", "48759587469854128745123654", Bigint{Number: []int16{1, 1, 0, 8, 2, 3, 8, 6, 4, 3, 9, 1, 8, 4, 0, 0, 7, 6, 7, 0, 9, 0, 6, 6, 2, 1}}, Bigint{Number: []int16{5, 2, 2, 3, 5, 6}}, nil},
		{"Test 6", "988847123412385995937737458960", "12", Bigint{Number: []int16{8}}, Bigint{Number: []int16{8, 2, 4, 0, 3, 9, 2, 6, 9, 5, 1, 0, 3, 2, 1, 6, 6, 3, 2, 8, 1, 4, 4, 7, 8, 8, 2, 4, 6}}, nil},
		{"Test 7", "988847123412385995937737458960", "1", Bigint{Number: []int16{0}}, Bigint{Number: []int16{9, 8, 8, 8, 4, 7, 1, 2, 3, 4, 1, 2, 3, 8, 5, 9, 9, 5, 9, 3, 7, 7, 3, 7, 4, 5, 8, 9, 6, 0}}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.label, func(t *testing.T) {
			a, _ := NewInt(tt.a)
			b, _ := NewInt(tt.b)
			ansD, ansM, err := DivitionMod(a, b)
			if err != tt.err {
				t.Errorf("Expected %v Got %v", tt.err, err)
			}
			for i := 0; i < len(ansD.Number); i++ {
				if ansD.Number[i] != tt.wantedD.Number[i] {
					t.Errorf("got %q wanted %q", ansD.Number, tt.wantedD.Number)
					break
				}

			}
			for i := 0; i < len(ansM.Number); i++ {
				if ansM.Number[i] != tt.wantedM.Number[i] {
					t.Errorf("got %q wanted %q", ansM.Number, tt.wantedM.Number)
					break
				}

			}
		})
	}

}

func TestAbs(t *testing.T) {
	tests := []struct {
		label  string
		num    string
		wanted Bigint
	}{
		{"Test 1", "10", Bigint{Number: []int16{1, 0}}},
		{"Test 2", "-10", Bigint{Number: []int16{1, 0}}},
		{"Test 3", "0", Bigint{Number: []int16{0}}},
	}
	for _, tt := range tests {
		t.Run(tt.label, func(t *testing.T) {
			a, _ := NewInt(tt.num)
			a.ABS()

			if a.Number[0] != tt.wanted.Number[0] {
				t.Errorf("got %q wanted %q", a.Number, tt.wanted.Number)

			}

		})
	}
}
