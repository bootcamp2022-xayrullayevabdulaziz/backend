package bigint

import (
	//	"fmt"

	"errors"
	"fmt"
	"strconv"
)

type Bigint struct {
	Number []int16
}

var ErrorMessage error = errors.New("bad input")

func NewInt(num string) (Bigint, error) {
	var a Bigint
	var err error
	var isMinus bool = false
	var isHead bool = true
	var isEmpty bool = false
	if num == "" {
		isEmpty = true

		return Bigint{Number: []int16{}}, ErrorMessage

	}
	for t, i := range num {
		if t == 0 && string(i) == "+" {
			continue

		}

		if t == 0 && string(i) == "-" {

			isMinus = true
			continue

		} else {
			var j int
			j, err = strconv.Atoi(string(i))
			if err != nil {

				return Bigint{Number: []int16{}}, ErrorMessage

			}

			if j == 0 && isHead {
				continue
			} else {
				isHead = false
			}
			if isMinus {
				a.Number = append(a.Number, int16(j)*(-1))
				isMinus = false
				continue
			}
			a.Number = append(a.Number, int16(j))

		}

	}

	if !isEmpty && len(a.Number) == 0 {
		a.Number = append(a.Number, 0)
	}

	return a, err
}
func (a *Bigint) Set(num string) (err error) {
	a.Number = nil

	var isMinus bool = false
	var isHead bool = true
	var isEmpty bool = false
	if num == "" {
		isEmpty = true
		err = ErrorMessage

	}
	for t, i := range num {
		if t == 0 && string(i) == "+" {
			continue

		}

		if t == 0 && string(i) == "-" {

			isMinus = true
			continue

		} else {
			var j int
			j, err = strconv.Atoi(string(i))
			if err != nil {
				return ErrorMessage
			}

			if j == 0 && isHead {
				continue
			} else {
				isHead = false
			}
			if isMinus {
				a.Number = append(a.Number, int16(j)*(-1))
				isMinus = false
				continue
			}
			a.Number = append(a.Number, int16(j))

		}

	}
	if !isEmpty && len(a.Number) == 0 {

		a.Number = append(a.Number, 0)

	}
	fmt.Println(a.Number, err)
	return err
}

func Add(sa, sb Bigint) Bigint {
	if sb.Number[0] == 0 && len(sb.Number) == 1 {
		return sa
	}
	var a, b Bigint
	var isBoothMinus bool = false
	a.Number = make([]int16, len(sa.Number))
	b.Number = make([]int16, len(sb.Number))
	copy(a.Number, sa.Number)
	copy(b.Number, sb.Number)
	if a.Number[0] > 0 && b.Number[0] < 0 {
		b.Number[0] = b.Number[0] * (-1)
		return Sub(a, b)
	}
	if a.Number[0] < 0 && b.Number[0] > 0 {
		a.Number[0] = a.Number[0] * (-1)
		return Sub(b, a)
	}
	if a.Number[0] < 0 && b.Number[0] < 0 {
		a.Number[0] = a.Number[0] * (-1)
		b.Number[0] = b.Number[0] * (-1)
		isBoothMinus = true
	}
	var result Bigint
	for i := 0; i < len(a.Number)/2; i++ {
		a.Number[i] = a.Number[i] - a.Number[len(a.Number)-i-1]
		a.Number[len(a.Number)-i-1] = a.Number[len(a.Number)-i-1] + a.Number[i]
		a.Number[i] = a.Number[len(a.Number)-i-1] - a.Number[i]
	}

	for i := 0; i < len(b.Number)/2; i++ {
		b.Number[i] = b.Number[i] - b.Number[len(b.Number)-i-1]
		b.Number[len(b.Number)-i-1] = b.Number[len(b.Number)-i-1] + b.Number[i]
		b.Number[i] = b.Number[len(b.Number)-i-1] - b.Number[i]
	}

	if len(a.Number) > len(b.Number) {
		distinguish := len(a.Number) - len(b.Number)

		for i := 0; i < distinguish; i++ {
			b.Number = append(b.Number, 0)
		}

		isHasRest := false
		var tempory int16
		for i := 0; i < len(a.Number); i++ {

			if isHasRest {
				tempory = a.Number[i] + b.Number[i] + 1
			} else {
				tempory = a.Number[i] + b.Number[i]

			}
			if tempory >= 10 {
				isHasRest = true
				result.Number = append(result.Number, tempory-10)
			} else {
				isHasRest = false
				result.Number = append(result.Number, tempory)
			}

		}

		if isHasRest {
			result.Number = append(result.Number, 1)
		}

	} else {
		distinguish := len(b.Number) - len(a.Number)
		for i := 0; i < distinguish; i++ {
			a.Number = append(a.Number, 0)
		}
		isHasRest := false
		var tempory int16
		for i := 0; i < len(a.Number); i++ {

			if isHasRest {
				tempory = a.Number[i] + b.Number[i] + 1
			} else {
				tempory = a.Number[i] + b.Number[i]

			}
			if tempory >= 10 {
				isHasRest = true
				result.Number = append(result.Number, tempory-10)
			} else {
				isHasRest = false
				result.Number = append(result.Number, tempory)
			}

		}
		if isHasRest {
			result.Number = append(result.Number, 1)
		}
	}

	for i := 0; i < len(result.Number)/2; i++ {
		result.Number[i] = result.Number[i] - result.Number[len(result.Number)-i-1]
		result.Number[len(result.Number)-i-1] = result.Number[len(result.Number)-i-1] + result.Number[i]
		result.Number[i] = result.Number[len(result.Number)-i-1] - result.Number[i]
	}

	if isBoothMinus {
		result.Number[0] = result.Number[0] * (-1)

	}

	return result
}
func Sub(sa, sb Bigint) Bigint {
	if sb.Number[0] == 0 && len(sb.Number) == 1 {
		return sa
	}
	var a, b Bigint
	a.Number = make([]int16, len(sa.Number))
	b.Number = make([]int16, len(sb.Number))
	copy(a.Number, sa.Number)
	copy(b.Number, sb.Number)
	if a.Number[0] > 0 && b.Number[0] < 0 {
		b.Number[0] = b.Number[0] * (-1)
		return Add(a, b)
	}
	if a.Number[0] < 0 && b.Number[0] > 0 {
		b.Number[0] = b.Number[0] * (-1)
		return Add(a, b)
	}
	if a.Number[0] < 0 && b.Number[0] < 0 {
		a.Number[0] = a.Number[0] * (-1)
		b.Number[0] = b.Number[0] * (-1)
		return Sub(b, a)
	}
	for i := 0; i < len(a.Number)/2; i++ {
		a.Number[i] = a.Number[i] - a.Number[len(a.Number)-i-1]
		a.Number[len(a.Number)-i-1] = a.Number[len(a.Number)-i-1] + a.Number[i]
		a.Number[i] = a.Number[len(a.Number)-i-1] - a.Number[i]
	}

	for i := 0; i < len(b.Number)/2; i++ {
		b.Number[i] = b.Number[i] - b.Number[len(b.Number)-i-1]
		b.Number[len(b.Number)-i-1] = b.Number[len(b.Number)-i-1] + b.Number[i]
		b.Number[i] = b.Number[len(b.Number)-i-1] - b.Number[i]
	}
	var result Bigint

	if len(a.Number) > len(b.Number) {
		distinguish := len(a.Number) - len(b.Number)
		for i := 0; i < distinguish; i++ {
			b.Number = append(b.Number, 0)
		}
		isNeedBorrow := false
		var tempory int16
		for i := 0; i < len(a.Number); i++ {

			if isNeedBorrow {
				tempory = a.Number[i] - b.Number[i] - 1
			} else {
				tempory = a.Number[i] - b.Number[i]

			}
			if tempory < 0 {
				isNeedBorrow = true
				result.Number = append(result.Number, tempory+10)
			} else {
				isNeedBorrow = false
				result.Number = append(result.Number, tempory)
			}

		}

	} else {

		if len(a.Number) == len(b.Number) {
			isNeedBorrow := false
			var tempory int16
			for i := 0; i < len(a.Number)/2; i++ {
				a.Number[i] = a.Number[i] - a.Number[len(a.Number)-i-1]
				a.Number[len(a.Number)-i-1] = a.Number[len(a.Number)-i-1] + a.Number[i]
				a.Number[i] = a.Number[len(a.Number)-i-1] - a.Number[i]
			}

			for i := 0; i < len(b.Number)/2; i++ {
				b.Number[i] = b.Number[i] - b.Number[len(b.Number)-i-1]
				b.Number[len(b.Number)-i-1] = b.Number[len(b.Number)-i-1] + b.Number[i]
				b.Number[i] = b.Number[len(b.Number)-i-1] - b.Number[i]
			}
			if Check(a, b) {
				for i := 0; i < len(a.Number)/2; i++ {
					a.Number[i] = a.Number[i] - a.Number[len(a.Number)-i-1]
					a.Number[len(a.Number)-i-1] = a.Number[len(a.Number)-i-1] + a.Number[i]
					a.Number[i] = a.Number[len(a.Number)-i-1] - a.Number[i]
				}

				for i := 0; i < len(b.Number)/2; i++ {
					b.Number[i] = b.Number[i] - b.Number[len(b.Number)-i-1]
					b.Number[len(b.Number)-i-1] = b.Number[len(b.Number)-i-1] + b.Number[i]
					b.Number[i] = b.Number[len(b.Number)-i-1] - b.Number[i]
				}
				for i := 0; i < len(a.Number); i++ {

					if isNeedBorrow {
						tempory = a.Number[i] - b.Number[i] - 1
					} else {
						tempory = a.Number[i] - b.Number[i]

					}
					if tempory < 0 {
						isNeedBorrow = true
						if i != len(a.Number)-1 {
							result.Number = append(result.Number, tempory+10)
							continue
						}
						result.Number = append(result.Number, tempory)
						for i := 0; i < len(result.Number)/2; i++ {
							result.Number[i] = result.Number[i] - result.Number[len(result.Number)-i-1]
							result.Number[len(result.Number)-i-1] = result.Number[len(result.Number)-i-1] + result.Number[i]
							result.Number[i] = result.Number[len(result.Number)-i-1] - result.Number[i]
						}
						return result
					} else {
						isNeedBorrow = false
						result.Number = append(result.Number, tempory)
					}

				}
			} else {
				fmt.Println("sdds")
				for i := 0; i < len(a.Number)/2; i++ {
					a.Number[i] = a.Number[i] - a.Number[len(a.Number)-i-1]
					a.Number[len(a.Number)-i-1] = a.Number[len(a.Number)-i-1] + a.Number[i]
					a.Number[i] = a.Number[len(a.Number)-i-1] - a.Number[i]
				}

				for i := 0; i < len(b.Number)/2; i++ {
					b.Number[i] = b.Number[i] - b.Number[len(b.Number)-i-1]
					b.Number[len(b.Number)-i-1] = b.Number[len(b.Number)-i-1] + b.Number[i]
					b.Number[i] = b.Number[len(b.Number)-i-1] - b.Number[i]
				}
				for i := 0; i < len(a.Number); i++ {

					if isNeedBorrow {
						tempory = b.Number[i] - a.Number[i] - 1
					} else {
						tempory = b.Number[i] - a.Number[i]

					}
					if tempory < 0 {
						isNeedBorrow = true
						if i != len(a.Number)-1 {
							result.Number = append(result.Number, tempory+10)
							continue
						}
						result.Number = append(result.Number, tempory)

					} else {
						isNeedBorrow = false
						result.Number = append(result.Number, tempory)
					}

				}
				for i := 0; i < len(result.Number)/2; i++ {
					result.Number[i] = result.Number[i] - result.Number[len(result.Number)-i-1]
					result.Number[len(result.Number)-i-1] = result.Number[len(result.Number)-i-1] + result.Number[i]
					result.Number[i] = result.Number[len(result.Number)-i-1] - result.Number[i]
				}

				for j := 0; j < len(result.Number)-1; j++ {

					if result.Number[j] != 0 {

						break
					} else {

						result.Number = result.Number[1:]
						j = -1

					}
				}

				fmt.Println(result)
				result.Number[0] = result.Number[0] * (-1)
				return result
			}

		} else {
			distinguish := len(b.Number) - len(a.Number)
			for i := 0; i < distinguish; i++ {
				a.Number = append(a.Number, 0)
			}
			isNeedBorrow := false
			var tempory int16
			for i := 0; i < len(b.Number); i++ {

				if isNeedBorrow {
					tempory = b.Number[i] - a.Number[i] - 1
				} else {
					tempory = b.Number[i] - a.Number[i]

				}
				if tempory < 0 {
					isNeedBorrow = true
					result.Number = append(result.Number, tempory+10)
				} else {
					isNeedBorrow = false
					result.Number = append(result.Number, tempory)
				}

			}
			result.Number[len(result.Number)-1] = result.Number[len(result.Number)-1] * (-1)
		}
	}

	for i := 0; i < len(result.Number)/2; i++ {
		result.Number[i] = result.Number[i] - result.Number[len(result.Number)-i-1]
		result.Number[len(result.Number)-i-1] = result.Number[len(result.Number)-i-1] + result.Number[i]
		result.Number[i] = result.Number[len(result.Number)-i-1] - result.Number[i]
	}

	for j := 0; j < len(result.Number)-1; j++ {

		if result.Number[j] != 0 {

			break
		} else {

			result.Number = result.Number[1:]
			j = -1

		}
	}

	return result

}
func Multiply(sa, sb Bigint) Bigint {
	var a, b Bigint
	if (sb.Number[0] == 0 || sa.Number[0] == 0) && (len(sb.Number) == 1 || len(sa.Number) == 1) {
		return Bigint{Number: []int16{0}}
	}
	a.Number = make([]int16, len(sa.Number))
	b.Number = make([]int16, len(sb.Number))
	copy(a.Number, sa.Number)
	copy(b.Number, sb.Number)
	var isMinusNumber bool = false
	if a.Number[0] > 0 && b.Number[0] < 0 {
		b.Number[0] = b.Number[0] * (-1)
		isMinusNumber = true
	}
	if a.Number[0] < 0 && b.Number[0] > 0 {
		a.Number[0] = a.Number[0] * (-1)
		isMinusNumber = true
	}
	if a.Number[0] < 0 && b.Number[0] < 0 {
		a.Number[0] = a.Number[0] * (-1)
		b.Number[0] = b.Number[0] * (-1)
		isMinusNumber = false
	}
	for i := 0; i < len(a.Number)/2; i++ {
		a.Number[i] = a.Number[i] - a.Number[len(a.Number)-i-1]
		a.Number[len(a.Number)-i-1] = a.Number[len(a.Number)-i-1] + a.Number[i]
		a.Number[i] = a.Number[len(a.Number)-i-1] - a.Number[i]
	}

	for i := 0; i < len(b.Number)/2; i++ {
		b.Number[i] = b.Number[i] - b.Number[len(b.Number)-i-1]
		b.Number[len(b.Number)-i-1] = b.Number[len(b.Number)-i-1] + b.Number[i]
		b.Number[i] = b.Number[len(b.Number)-i-1] - b.Number[i]
	}
	var result Bigint
	var result1 Bigint
	isHasRest := 0
	var tempory int16

	for j := 0; j < len(a.Number); j++ {

		tempory = a.Number[j]*b.Number[0] + int16(isHasRest)

		if tempory >= 10 {

			isHasRest = int(tempory) / 10
			result1.Number = append(result1.Number, tempory%10)
		} else {
			isHasRest = 0
			result1.Number = append(result1.Number, tempory)
		}
	}
	if isHasRest != 0 {
		result1.Number = append(result1.Number, int16(isHasRest))
	}
	for t := 0; t < len(result1.Number)/2; t++ {
		result1.Number[t] = result1.Number[t] - result1.Number[len(result1.Number)-t-1]
		result1.Number[len(result1.Number)-t-1] = result1.Number[len(result1.Number)-t-1] + result1.Number[t]
		result1.Number[t] = result1.Number[len(result1.Number)-t-1] - result1.Number[t]
	}

	for i := 1; i < len(b.Number); i++ {
		isHasRest = 0

		for j := 0; j < len(a.Number); j++ {

			tempory = a.Number[j]*b.Number[i] + int16(isHasRest)

			if tempory >= 10 {

				isHasRest = int(tempory) / 10
				result.Number = append(result.Number, tempory%10)
			} else {
				isHasRest = 0
				result.Number = append(result.Number, tempory)
			}
		}
		if isHasRest != 0 {
			result.Number = append(result.Number, int16(isHasRest))
		}

		for m := 0; m < len(result.Number)/2; m++ {
			result.Number[m] = result.Number[m] - result.Number[len(result.Number)-m-1]
			result.Number[len(result.Number)-m-1] = result.Number[len(result.Number)-m-1] + result.Number[m]
			result.Number[m] = result.Number[len(result.Number)-m-1] - result.Number[m]
		}

		for q := 0; q < i; q++ {
			result.Number = append(result.Number, 0)
		}

		result1 = Add(result1, result)

		result.Number = nil
	}
	if isMinusNumber {
		result1.Number[0] = result1.Number[0] * (-1)
	}
	return result1
}
func isSame(a, b Bigint) bool {
	if len(a.Number) != len(b.Number) {
		return false
	}
	for i := range a.Number {
		if a.Number[i]!=b.Number[i]{
			return false
		}
	}
	return true
}
func Check(sa, sb Bigint) bool {
	var a, b Bigint

	a.Number = make([]int16, len(sa.Number))
	b.Number = make([]int16, len(sb.Number))
	copy(a.Number, sa.Number)
	copy(b.Number, sb.Number)

	if a.Number[len(a.Number)-1] < 0 && b.Number[len(b.Number)-1] > 0 {
		return false
	}
	if a.Number[len(a.Number)-1] > 0 && b.Number[len(b.Number)-1] < 0 {
		return true
	}

	if len(a.Number) > len(b.Number) {
		return true

	} else {
		if len(a.Number) < len(b.Number) {
			return false
		} else {

			for i := 0; i < len(a.Number)/2; i++ {
				a.Number[i] = a.Number[i] - a.Number[len(a.Number)-i-1]
				a.Number[len(a.Number)-i-1] = a.Number[len(a.Number)-i-1] + a.Number[i]
				a.Number[i] = a.Number[len(a.Number)-i-1] - a.Number[i]
			}

			for i := 0; i < len(b.Number)/2; i++ {
				b.Number[i] = b.Number[i] - b.Number[len(b.Number)-i-1]
				b.Number[len(b.Number)-i-1] = b.Number[len(b.Number)-i-1] + b.Number[i]
				b.Number[i] = b.Number[len(b.Number)-i-1] - b.Number[i]
			}
			isNeedBorrow := false

			var tempory int16
			for i := 0; i < len(a.Number); i++ {

				if isNeedBorrow {
					tempory = a.Number[i] - b.Number[i] - 1
				} else {
					tempory = a.Number[i] - b.Number[i]

				}

				if tempory < 0 {
					isNeedBorrow = true

				} else {
					isNeedBorrow = false

				}
			}

			if isNeedBorrow {
				return false
			} else {
				return true
			}

		}

	}

}
func Mod(a, b Bigint) (divition, mod Bigint, err error) {
	var r Bigint

	divition.Number = append(divition.Number, 0)
	r.Number = append(r.Number, 1)

	for Check(a, b) {

		a = Sub(a, b)

		divition = Add(divition, r)

		if a.Number[0] == 0 && len(a.Number) == 1 {
			mod = a

			return divition, mod, nil
		}
		if a.Number[0] == 0 {
			a.Number = a.Number[1:len(a.Number)]
		}

	}

	mod = a
	fmt.Println("kk", divition, mod)
	return divition, mod, nil
}

func DivitionMod(a, b Bigint) (divition, mod Bigint, err error) {
	if b.Number[0] == 0 && len(b.Number) == 1 {
		return Bigint{Number: []int16{}}, Bigint{Number: []int16{}}, ErrorMessage
	}
	if a.Number[0] == 0 && len(a.Number) == 1 {
		return Bigint{Number: []int16{0}}, Bigint{Number: []int16{0}}, nil
	}
	var lenMod int = len(b.Number)
	var isMinusDiv bool = false
	var isMinusMod bool = false
	var r Bigint
	if a.Number[0] < 0 && b.Number[0] > 0 {
		isMinusDiv = true
		isMinusMod = true
		a.Number[0] = a.Number[0] * (-1)
	}
	if a.Number[0] > 0 && b.Number[0] < 0 {
		isMinusDiv = true
		isMinusMod = false
		b.Number[0] = b.Number[0] * (-1)
	}
	if a.Number[0] < 0 && b.Number[0] < 0 {
		isMinusMod = true
		isMinusDiv = false
		a.Number[0] = a.Number[0] * (-1)
		b.Number[0] = b.Number[0] * (-1)
	}
	if !Check(a, b) {
		divition, mod, err = Mod(a, b)

	}
	for Check(a, b) {

		if len(a.Number) == len(b.Number) {
			d1, m1, _ := Mod(a, b)
			divition.Number = append(divition.Number, d1.Number...)
			a = m1
			break
		}
		r.Number = a.Number[0 : lenMod+1]

		d1, m1, _ := Mod(r, b)

		divition.Number = append(divition.Number, d1.Number...)

		if m1.Number[0] == 0 {

			a.Number = a.Number[lenMod+1:]

			for p := 0; p < len(a.Number); p++ {
				if a.Number[p] == 0 {
					divition.Number = append(divition.Number, 0)
					a.Number = a.Number[1:]
					p = -1
				} else {
					break
				}

			}
			if len(a.Number) == 0 {
				a.Number = append(a.Number, -1)
				break
			}
			var y Bigint
			y.Number = a.Number[0:1]

			if Check(b, y) {
				if !isSame(b,y){
					divition.Number = append(divition.Number, 0)
				}
				

			}

			continue
		}

		a.Number = append(m1.Number, a.Number[lenMod+1:]...)

	}
	if a.Number[0] == -1 {
		a.Number[0] = 0
	}
	mod = a
	if isMinusMod {

		mod.Number[0] = mod.Number[0] * (-1)
	}
	if isMinusDiv {

		divition.Number[0] = divition.Number[0] * (-1)
	}

	return divition, mod, err
}
func (a *Bigint) ABS() {
	if a.Number[0] < 0 {
		a.Number[0] = a.Number[0] * (-1)
	}

}
