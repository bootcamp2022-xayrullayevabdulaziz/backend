const path=require('path')
const fs=require('fs')
const express = require('express')
const app=require('express')()
app.use(express.static('public'));
app.use(express.json())

app.post('/',(req,res)=>{
    let urlPath=path.join(__dirname,req.body.pathOfFile)
    

  console.log('body :', req);
    let pro=new Promise((resolved,reject)=>{
        fs.readFile(urlPath,'utf8',(err,data)=>{
            if(err!=null) {reject()}
            else{
                resolved(data)
            }
        })


    }) 
    pro.then((data)=>res.send(data)).catch((err)=>res.send("There is no file as this path=>"+ urlPath))
})
app.listen(3000,()=>{console.log("server running on 3000 port")})
