CREATE DATABASE blogpost;

CREATE TABLE article (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    published_year DATE NOT NULL,
    cost VARCHAR(50) NOT NULL,
    number_of_audence INT
);

create table author (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    gender VARCHAR(50),
    country VARCHAR(50),
    birth_of_date DATE,
    article_id BIGINT REFERENCES article(id),
    UNIQUE(article_id)
);

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Reilly',
        'Shirlaw',
        'Male',
        'Mexico',
        '02/10/1951'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Rabi',
        'Davidsohn',
        'Male',
        'China',
        '18/09/1954'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Terrell',
        'Minchinton',
        'Male',
        'Mexico',
        '17/04/1969'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Archy',
        'Goudman',
        'Male',
        'Philippines',
        '02/03/1999'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Benyamin',
        'Morsley',
        'Male',
        'Japan',
        '07/06/1950'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Lynde',
        'Tittletross',
        'Female',
        'Russia',
        '02/04/1979'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Wendie',
        'Woodlands',
        'Female',
        'Japan',
        '04/02/1954'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    ('Tani', 'Seid', 'Female', 'Brazil', '04/04/1951');

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Fiorenze',
        'Kopje',
        'Female',
        'China',
        '23/09/1992'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Worthy',
        'Michieli',
        'Male',
        'Indonesia',
        '24/11/1968'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Nickey',
        'Sisselot',
        'Male',
        'Colombia',
        '27/09/1979'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Fabio',
        'Cornock',
        'Male',
        'Uganda',
        '01/05/1978'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Scot',
        'Elbourne',
        'Male',
        'Indonesia',
        '08/11/1966'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Selena',
        'Hay',
        'Female',
        'Ecuador',
        '15/01/1951'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Keven',
        'Pennings',
        'Male',
        'Indonesia',
        '29/10/1965'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Craig',
        'Worthington',
        'Male',
        'Poland',
        '17/02/1997'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Berti',
        'Storror',
        'Male',
        'Colombia',
        '24/11/1997'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Tripp',
        'Steffens',
        'Male',
        'United States',
        '13/12/1973'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Wandis',
        'Mathe',
        'Female',
        'Brazil',
        '31/03/1997'
    );

insert into
    author (
        first_name,
        last_name,
        gender,
        country,
        birth_of_date
    )
values
    (
        'Gayle',
        'Delnevo',
        'Female',
        'Philippines',
        '06/11/1954'
    );

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Voltsillam', '08/04/2004', '$20.09', 20);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Kanlam', '01/05/1992', '$37.99', 48);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Sub-Ex', '09/10/2012', '$46.23', 94);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Aerified', '14/04/2016', '$6.66', 76);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Simba', '06/01/2015', '$90.27', 48);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Bamity', '19/09/2016', '$39.50', 65);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Matsoft', '09/11/1991', '$54.96', 91);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Konklab', '09/09/1997', '$56.61', 59);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Tempsoft', '29/09/2000', '$16.71', 22);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Zaam-Dox', '27/04/1994', '$86.91', 66);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Tres-Zap', '10/02/1993', '$39.81', 77);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Cardguard', '08/05/1991', '$83.04', 99);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Span', '06/05/2011', '$79.84', 13);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Cookley', '09/03/2007', '$38.78', 93);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Fix San', '07/10/2005', '$66.44', 19);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Lotstring', '20/04/1991', '$65.81', 18);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Sub-Ex', '15/01/1992', '$47.12', 70);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Y-find', '31/05/2013', '$24.48', 52);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Span', '25/01/2000', '$63.74', 48);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Konklux', '24/09/2002', '$94.88', 16);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Tres-Zap', '17/03/1993', '$37.36', 60);

insert into
    article (title, published_year, cost, number_of_audence)
values
    ('Stronghold', '23/01/2020', '$62.52', 64);

SELECT
    *
from
    author
wHERE
    gender = 'Male';

SELECT
    *
from
    author
wHERE
    gender = 'Male' offset 3;

SELECT
    *
from
    author
wHERE
    gender = 'Male'
limit
    5;

SELECT
    *
from
    author
order by
    birth_of_date;

SELECT
    *
from
    article
wHERE
    number_of_audence > 50;

SELECT
    *
from
    article
wHERE
    number_of_audence > 50 offset 3;

SELECT
    *
from
    article
wHERE
    number_of_audence > 50
limit
    7;

SELECT
    *
from
    article
order by
    title;

UPDATE
    author
SET
    article_id = 5
WHERE
    id = 7;

UPDATE
    author
SET
    article_id = 10
WHERE
    id = 4;

UPDATE
    author
SET
    article_id = 14
WHERE
    id = 8;

UPDATE
    author
SET
    article_id = 20
WHERE
    id = 10;

UPDATE
    author
SET
    article_id = 8
WHERE
    id = 2;

UPDATE
    author
SET
    article_id = 9
WHERE
    id = 18;

UPDATE
    author
SET
    article_id = 17
WHERE
    id = 12;

SELECT
    *
FROM
    author
    JOIN article ON author.article_id = article.id;