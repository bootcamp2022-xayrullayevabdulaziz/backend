# To Create "blogpost" db
![aa](./images/createDb.png)
***
## show list of existing databases
![aa](./images/showDBlist.png)
***
## then list of db will appear
![aa](./images/listDB.png)
***
## then we should switch db from "postgres" to "blogpost"
![aa](./images/switchDb.png)
***
# Create "article"  table
![aa](./images/createArticleTable.png)
***
# Create "author"  table
![aa](./images/createAuthorTable.png)