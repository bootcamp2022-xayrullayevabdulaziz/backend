const app=require('express')()
var books=require('./books')
const uuid=require('uuid')
const express = require('express')
//bodydagi jsondi o'qish uchun 
app.use(express.json())


// get all date
app.get('/',(req,res)=>{
    res.send(books)
})

// get a book which is selected by user(read)
app.get('/api/books/:id',(req,res)=>{
    books=books.filter(book=>book.id==req.params.id)
    res.send(books)
})
// add book which is given by user(post)
app.post('/',(req,res)=>{
    if(!req.body.name||!req.body.published||!req.body.cost){
        return   res.status(404).json({message:"hamma ma'lumotlarni to'ldiring"})
       }
       
        var newBook={
            id:uuid.v4(),
            name:req.body.name,
            published:req.body.published,
            cost:req.body.cost
        }
        books.push(newBook)
        res.json(books)
       
 
 
    
   
})
// edit book's date by using selected id
app.put('/api/books/:id',(req,res)=>{
    books.forEach((book)=>{
        if(book.id==req.params.id){
            book.name=req.body.name
            book.published=req.body.published
            book.cost=req.body.cost
        }
    })
    res.send(books)
})
// delete book by using selected id
app.delete('/api/books/:id',(req,res)=>{
    books=books.filter(book=>book.id!=req.params.id)
    res.send(books)
})
app.listen(3000,()=>{console.log("server running...")})
























// const express=require('express')
// const path =require('path')
// const app=express()
// const books=require('./books')
// const uuid=require('uuid')
// app.use(express.json())

// app.post('/api/books/',(req,res)=>{
//     if(!req.body.name || !req.body.published||!req.body.cost){
//        return res.status(404).json({message:"hamma malumotni to'g'ri kirit"})
//     }
//     var newBook={
//         id:uuid.v4(),
//         name:req.body.name,
//         published:req.body.published,
//         cost:req.body.cost}
//     books.push(newBook)
    
//     res.json(books)
    
// })
// app.listen(3000,()=>{console.log("server runing")})