package controllers

import (
	
	
	"net/http"
	"usingsqlx/db_client"
	"usingsqlx/moduls"

	"github.com/gin-gonic/gin"
)

func CreateBook(c *gin.Context) {
	var reqBody moduls.Book

	if err := c.ShouldBindJSON(&reqBody); err != nil {

		c.JSON(http.StatusBadRequest, gin.H{
			"error":     true,
			"myOpinion": "invalid request body",
			"message":   err,
		})
		return
	}

	tx := dbclient.DBClient.MustBegin()

	tx.MustExec("INSERT INTO books (title) VALUES ($1)", reqBody.Title)
	_, err := tx.NamedExec("INSERT INTO Book_category (janr,audence) VALUES (:janr,:audence)", &moduls.Book{
		Janr:    reqBody.Janr,
		Audence: reqBody.Audence,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error":   true,
			"message": "couldn't added",
		})
	}

	var a int

	dbclient.DBClient.Get(&a, "SELECT  id FROM books ORDER BY id DESC LIMIT 1")
	var b int

	dbclient.DBClient.Get(&b, "SELECT  id FROM Book_category ORDER BY id DESC LIMIT 1")
	tx.MustExec("INSERT INTO BooksJoinCategories (BookId, CategoryId) VALUES ($1,$2)", a+1, b+1)
	tx.Commit()
	c.JSON(http.StatusCreated, gin.H{
		"error":   false,
		"message": "successfuly added",
	
	})

}
func GetBooks(c *gin.Context) {
	var books []moduls.Book

	err := dbclient.DBClient.Select(&books, "SELECT  ba.BookId,b.Title ,a.Janr ,a.Audence as audence FROM BooksJoinCategories ba INNER JOIN Book_category a ON a.id = ba.CategoryId INNER JOIN Books b ON b.id = ba.bookid ;")
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error":     true,
			"massage": "not data",
			"message":   err,
		})
	}

	c.JSON(http.StatusOK, books)
}

func GetBook(c *gin.Context) {
	id := c.Param("id")
	var book moduls.Book
	err := dbclient.DBClient.Get(&book, "SELECT  ba.BookId,b.Title ,a.Janr ,a.Audence as audence FROM BooksJoinCategories ba INNER JOIN Book_category a ON a.id = ba.CategoryId INNER JOIN Books b ON b.id = ba.bookid  where bookid=$1;", id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error":   true,
			"message": "this bookId doesn't exist",
		})
		return
	}
	c.JSON(http.StatusOK, book)
}
func UpdateBook(c *gin.Context) {
	var reqBody moduls.Book

	if err := c.ShouldBindJSON(&reqBody); err != nil {

		c.JSON(http.StatusBadRequest, gin.H{
			"error":     true,
			"myOpinion": "invalid request body",
			"message":   err,
		})
		return
	}
	var selectedCategoryId int
	err := dbclient.DBClient.Get(&selectedCategoryId, "SELECT  ba.categoryid FROM BooksJoinCategories ba INNER JOIN Book_category a ON a.id = ba.CategoryId INNER JOIN Books b ON b.id = ba.bookid  where bookid=$1;", reqBody.BookId)
	if err != nil {
		
		c.JSON(http.StatusNotFound, gin.H{
			"error":     true,
			"myOpinion": "book which selected id doesn't exist",
			"message":   err,
		})
		return
	}

	tx := dbclient.DBClient.MustBegin()
	tx.MustExec("UPDATE books SET title=$1 WHERE id=$2;", reqBody.Title, reqBody.BookId)
	tx.MustExec("UPDATE Book_category SET janr=$1 ,audence=$2 WHERE id=$3;", reqBody.Janr, reqBody.Audence,selectedCategoryId)
	tx.Commit()
	c.JSON(http.StatusOK, gin.H{
		"error":   false,
		"message": "successfuly edited",
	
	})
	

}

func DeleteBook(c *gin.Context) {
	id := c.Param("id")

	var selectedCategoryId int
	err := dbclient.DBClient.Get(&selectedCategoryId, "SELECT  ba.categoryid FROM BooksJoinCategories ba INNER JOIN Book_category a ON a.id = ba.CategoryId INNER JOIN Books b ON b.id = ba.bookid  where bookid=$1;", id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error":   true,
			"message": "this bookId doesn't exist",
		})
		return
	}
	tx := dbclient.DBClient.MustBegin()
	tx.MustExec("DELETE FROM BooksJoinCategories WHERE bookid=$1;", id)
	tx.MustExec("DELETE FROM books WHERE id=$1;",id)
	//tx.MustExec("DELETE FROM Book_category  WHERE id=$1 LIMIT 1;", selectedCategoryId)
	
	tx.Commit()
	c.JSON(http.StatusOK, gin.H{
		"error":   false,
		"message": "successfuly deleted",
	
	})
}

