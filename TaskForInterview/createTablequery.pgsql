CREATE TABLE Books (
    Id BIGSERIAL NOT NULL ,
    Title VARCHAR(50) NOT NULL,
    PRIMARY KEY(Id)
);
CREATE TABLE Book_category (
    Id BIGSERIAL NOT NULL ,
    Janr VARCHAR(70) NOT NULL,
    audence VARCHAR(100) NOT NULL,
    PRIMARY KEY(Id)
);



CREATE TABLE BooksJoinCategories (
    CategoryId INT NOT NULL,
    BookId  INT NOT NULL,
    FOREIGN KEY (CategoryId) REFERENCES Book_category(Id),
    FOREIGN KEY (BookId) REFERENCES Books(Id)
);

INSERT INTO Book_category
    (Janr, Audence)
VALUES
    ('dramma', 'adult'),
    ('melodramma', 'older'),
    ('tagedya', 'adult'),
    ('comedy', 'young'),
    ('religios', 'adult'),
    ('fantastic', 'young'),
    ('horror', 'young'),
    ('Classics', 'older')
;

INSERT INTO Books
    (Title)
VALUES
    ('The Catcher in the Rye'),
    ('Nine Stories'),
    ('Franny and Zooey'),
    ('The Great Gatsby'),
    ('Tender id the Night'),
    ('Pride and Prejudice'),
    ('Professional ASP.NET 4.5 in C# and VB')
;

INSERT INTO BooksJoinCategories
    (BookId, CategoryId)
VALUES
    (1, 1),
    (2, 1),
    (3, 1),
    (4, 2),
    (5, 2),
    (6, 3),
    (7, 4)
    
;