package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger" 
	_ "usingsqlx/docs"
	"usingsqlx/controllers"
	dbclient "usingsqlx/db_client"
)

func main() {

	dbclient.InitialiseDBConnection()

	r := gin.Default()

	PageRoutes := r.Group("/book")
	{
		PageRoutes.POST("/", controllers.CreateBook)
		PageRoutes.GET("/", controllers.GetBooks)
		PageRoutes.GET("/:id", controllers.GetBook)
		PageRoutes.DELETE("/:id", controllers.DeleteBook)
		PageRoutes.PUT("/", controllers.UpdateBook)
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	if err := r.Run(":3000"); err != nil {
		panic(err.Error())
	}

}
