package dbclient

import (
	
	"log"

	"github.com/jmoiron/sqlx"
)

var DBClient *sqlx.DB
func InitialiseDBConnection()  {
	db, err := sqlx.Connect("postgres", "user=postgres password=7007 dbname=library sslmode=disable")
    if err != nil {
        log.Fatalln(err)
    }
	DBClient=db
	
}