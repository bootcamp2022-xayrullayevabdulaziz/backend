#!/usr/bin/bash

for ((j=0;j<3;j++))
do 
 for ((i=0;i<4;i++))
do 
 touch "$(date '+%M-%S')_file"
echo "$i" >> $(date '+%M-%S')_file
sleep 1
done
done


FILES=$(ls *file)

for file1 in $FILES; do
    for file2 in $FILES; do
        # echo "checking $file1 and $file2"
        if [[ "$file1" != "$file2" && -e "$file1" && -e "$file2" ]]; then
            if diff "$file1" "$file2" > /dev/null; then
                # echo "$file1 and $file2 are duplicates"
                rm -v "$file2"
            fi
        fi
    done
done


