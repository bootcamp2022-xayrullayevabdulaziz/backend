package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type User struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Age    int    `json:"age"`
	Salary int    `json:"salary"`
}

var Users []User



func main() {
	r := gin.Default()
	userRoutes := r.Group("/users")
	{
		userRoutes.GET("/", getUsers)
		userRoutes.POST("/", postUser)
		userRoutes.PUT("/:id", editUser)
		userRoutes.DELETE("/:id", deleteUser)
	}

	if err := r.Run(":3000"); err != nil {
		log.Fatal(err.Error())
	}
}
func getUsers(c *gin.Context) {
	c.JSON(200, Users)
}
func postUser(c *gin.Context){
	var reqBody User
	if err:=c.ShouldBindJSON(&reqBody);err!=nil{
		c.JSON(422,gin.H{
			"error":true,
			"message":"invalid request body",
		})
		return
	} 
	reqBody.ID=uuid.New().String()
	Users=append(Users, reqBody)
	c.JSON(200,gin.H{
		"error":false,
	})
}
func editUser(c *gin.Context){
	id:=c.Param("id")
	var reqBody User
	if err:=c.ShouldBindJSON(&reqBody);err!=nil{
		c.JSON(422,gin.H{
			"error":true,
			"message":"invalid request body",
		})
		return
	} 
	for i,user:=range Users{
		if user.ID==id{
			Users[i].Name=reqBody.Name
			Users[i].Age=reqBody.Age
			Users[i].Salary=reqBody.Salary
			c.JSON(200,gin.H{
				"error":false,
			})
			return
		}
	}
	c.JSON(404,gin.H{
		"error":true,
		"message":"invalid user id",
	})
}
func deleteUser(c *gin.Context){
	id:=c.Param("id")
	for i,user:=range Users{
		if user.ID==id{
			Users=append(Users[:i],Users[i+1:]... )
			c.JSON(200,gin.H{
				"error":false,
			})
			return

		}

	}
	c.JSON(404,gin.H{
		"error":true,
		"message":"invalid user id",
	})
}