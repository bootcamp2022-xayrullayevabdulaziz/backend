# Terminal Commands
 - ## cd folderName 
  enter the folder which is given
![a](./picture/cd.png)

   [open file](./2022-04-18---17-46-42)

 - ## ls  
  open variants

![a](./picture/ls.png)

   [open file](./2022-04-18---17-49-36)

   - ## which fileName 
   
   path of those
  


![a](./picture/which.png)

   [open file](./2022-04-18---17-51-15)


   - ## mkdir folderName 
   create folder
   
  


![a](./picture/mkdir.png)

   [open file](./2022-04-18---17-52-32)

   - ## rmdir folderName
   remove completly empty folder
    

- ## rm -rf folderName
delete folder, it doesn't mattter empty or not

   
   
  


![a](./picture/rmdir.png)

   [open file](./2022-04-18---17-53-24)


 - ## pwd 
 inform absolute address

   
   
  


![a](./picture/pwd.png)

   [open file](./2022-04-18---17-55-06)


 - ## man someComand
    
    definition comand


   
   
  


![a](./picture/man.png)
![a](./picture/man1.png)

   [open file](./2022-04-18---17-55-51)


## clear 
clear terminal

![a](./picture/clear.png)
![a](./picture/clear1.png)

[open file](./2022-04-18---17-56-43)


## cp 
copy file

![a](./picture/copy.png)


[open file](./2022-04-18---17-57-21)

## mv
move file

![a](./picture/mv.png)


[open file](./2022-04-18---17-59-58)

## cat fileName
open the text of file in order to read only

![a](./picture/cat.png)


[open file](./2022-04-18---18-00-34)

## some command ; another command
it used to work two command together


![a](./picture/appendCommands.png)

[open file](./2022-04-18---18-01-07)


## less
it used to read large text file
[open file](./2022-04-18---18-01-42)
## grep "some text" fileName
search text in a file
![a](./picture/grep.png)

[open file](./2022-04-18---18-02-19)

## history
show history of command
![a](./picture/history.png)

[open file](./2022-04-18---18-02-41)

## ps 
report a snapshot of the current processes
![a](./picture/ps.png)
## ps -aux
open dispetcher zadach
![a](./picture/aux1.png)


[open file](./2022-04-18---18-03-27)

## top
display Linux processes
![a](./picture/top.png)


[open file](./2022-04-18---18-04-28)
## kill
send a signal to a process
![a](./picture/kill.png)


[open file](./2022-04-18---18-05-01)

## passwd
change password.
## NOTE! 
only accepted strong password
[open file](./2022-04-18---18-05-26)
