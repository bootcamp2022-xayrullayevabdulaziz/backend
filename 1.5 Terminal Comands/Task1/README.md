# Task1
- ## Write a command that searches files of a folder

```
find Documents -type f -name salom.txt
```
This command can search salom.txt in Documents folder

- If this file exist in this folder, it reveals the path of salom.txt
![t](./task1.png)
- If this file doesn't exist in this folder, it reveals nothing
![t](./task1.1.png)
## NOTE!
You are in home directory
