package multiplymatrix

import "fmt"

func MultiplyMatrix() {
	const N int = 5

	multiA := [N][N]int{
		{19, 21, 45, 74, 10},
		{29, 46, 41, 4, 3},
		{1, 4, 7, 5, 9},
		{2, 6, 1, 5, 8},
		{8, -1, 5, 8, 2},
	}
	multiB := [N][N]int{
		{6, 1, 40, 71, -5},
		{-3, 4, 1, -4, 3},
		{4, -8, -7, 0, 3},
		{3, 4, -1, -5, 7},
		{2, 1, -3, 4, 7},
	}
	var result [N][N]int
	for i := 0; i < N; i++ {
		for j := 0; j < N; j++ {
			s := 0
			for t := 0; t < N; t++ {
				s = s + multiA[i][t]*multiB[t][j]
			}
			result[i][j] = s
		}
	}
	for i := 0; i < N; i++ {
		for j := 0; j < N; j++ {
			
			fmt.Printf("%d   ",result[i][j])
		}
		fmt.Println()
		fmt.Println()
	}
	
}
