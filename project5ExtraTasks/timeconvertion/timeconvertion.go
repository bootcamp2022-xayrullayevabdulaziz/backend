package timeconvertion

import (
	"errors"
	"strconv"
	"strings"
)



func TimeModify(text string) string{
	if len(text)!=10{
		panic(errors.New("to'g'ri formatda kiriting"))
	}
	if text[len(text)-2]=='A'{

		if text[0:2]=="12"{
			text=replaceAtIndex(text,'0',0)
			text=replaceAtIndex(text,'0',1)
			
		}
		return text[0:len(text)-2]
	}else{
		if text[0:2]=="12"{
			return text[0:len(text)-2]
			
		}
		j, err := strconv.Atoi(string(text[0:2]))
		if err != nil {
			panic(err)
		}
		j=j+12
		
		text=strconv.Itoa(j)+text[2:]
		return text[0:len(text)-2]
		
	}
	
	
}
func replaceAtIndex(input string, replacement byte, index int) string {
    return strings.Join([]string{input[:index], string(replacement), input[index+1:]}, "")
}